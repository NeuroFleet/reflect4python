import logging

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


from hooks.RDF import SparqlHook

class SparqlOperator(BaseOperator):
    template_fields = ('sparql',)
    template_ext = ('.sparql',)
    ui_color = '#ededed'

    @apply_defaults
    def __init__(
            self, sparql, sparql_conn_id='sparql_default', parameters=None,
            autocommit=False, *args, **kwargs):
        super(SparqlOperator, self).__init__(*args, **kwargs)
        self.sparql_conn_id = sparql_conn_id
        self.sparql = sparql
        self.autocommit = autocommit
        self.parameters = parameters

    def execute(self, context):
        logging.info('Executing: ' + str(self.sparql))
        hook = SparqlHook(sparql_conn_id=self.sparql_conn_id)
        hook.run(
            self.sparql,
            autocommit=self.autocommit,
            parameters=self.parameters)

class SparqlOperator(Operator):
    pass

