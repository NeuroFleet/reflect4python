from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner

################################################################################

class Implant(ApplicationSession):
    def trigger(self, method, *args, **kwargs):
        callback = getattr(self, method, None)

        if callable(callback):
            return callback(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def print_result(self, method, res, ex):
        if ex:
            print("call result: {}".format(res))
        else:
            print("call result: {}".format(res))

    ############################################################################

    def route(self, nrw):
        return nrw

    #***************************************************************************


    ############################################################################

    def invoke(self): return lambda hnd: self.call(hnd, *args, **kwargs)
    def topic(self):  return lambda hnd: self.subscription(hnd, *args, **kwargs)

    #***************************************************************************

    def call(self, callback, method, *args, **kwargs):
        target = self.route(method)

        try:
            res = yield self.call(unicode(target), *args, **kwargs)

            callback(target, res, None)
        except Exception as e:
            print("could not call procedure '{0}' : {1}".format(target, ex))

            return None

    #***************************************************************************

    def register(self, callback, method):
        target = self.route(method)

        try:
            yield self.register(callback, unicode(target))

            print("procedure '%s' registered." % target)
        except Exception as ex:
            print("could not register procedure '{0}' : {1}".format(target, ex))

    #***************************************************************************

    def subscription(self, callback, narrow):
        target = self.route(narrow)

        try:
            yield self.subscribe(callback, unicode(target))

            print("subscribed to topic '%s'." % target)
        except Exception as ex:
            print("could not subscribe to topic {0} : {1}".format(target, ex))

    ############################################################################

    @inlineCallbacks
    def onJoin(self, details):
        self.trigger('prepare')

        self.trigger('begin')

        counter = 0
        while True:
            self.trigger('before')

            self.trigger('process')

            self.trigger('after')

        self.trigger('finish')

    ############################################################################

    @classmethod
    def commandline(self, hnd):
        pass

