from flask import Flask, session, redirect, url_for, escape, request, render_template

import os, sys, re

app = Flask(os.environ['SLACK_BOT'])

app.debug = True
app.instance_path = os.path.dirname(os.path.dirname(__file__))
app.secret_key    = os.environ.get('SESSION_SECRET', 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT')

################################################################################

app.jinja_options = {
    'extensions': [
        'jinja2.ext.autoescape',
        'jinja2.ext.do',
        'jinja2.ext.loopcontrols',
        'jinja2.ext.with_',
    ],
}

#*******************************************************************************

@app.context_processor
def inject_user():
    return dict(hello='world')

@app.context_processor
def utility_processor():
    def static_assets(*path):
        return '/'.join(['/static']+[x for x in path])

    def format_price(amount, currency=u'$'):
        return u'{0:.2f}{1}'.format(amount, currency)

    return dict(
        currency = format_price,
        static   = static_assets,
    )

#*******************************************************************************

class Widget(object):
    def __init__(self, template_path, title, **attrs):
        self._tpl = template_path + '.html'
        self._lbl = title
        self._hdn = None
        self._kws = attrs

    template = property(lambda self: self._tpl)

    title    = property(lambda self: self._lbl)
    heading  = property(lambda self: self._hdn or '')

    def __getitem__(self, key, default=None): return self._kws.get(key, default)

    def render(self):
        cnt = dict([(k,v) for k,v in self._kws.iteritems()])

        tpl = app.jinja_env.get_template(self.template)

        cnt['wdg']    = self
        cnt['widget'] = self

        cnt['data']   = self

        return tpl.render(cnt)

################################################################################

from celery import Celery

def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

app.config.update(
    CELERY_BROKER_URL=os.environ.get('CLOUDAMQP_URL', 'redis://localhost:6379'),
    CELERY_RESULT_BACKEND=os.environ.get('REDISCLOUD_URL', 'redis://localhost:6379'),
)

celery = make_celery(app)

################################################################################

class View(object):
    def __init__(self, *args, **kwargs):
        if callable(getattr(self, 'initialize', None)):
            self.initialize(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if callable(getattr(self, 'invoke', None)):
            return self.invoke(*args, **kwargs)

        return None

################################################################################

class Resource(View):
    def initialize(self, *args, **kwargs):
        pass

    def invoke(self, *args, **kwargs):
        pass

################################################################################

class Adapter(View):
    def invoke(self, *args, **kwargs):
        pass

