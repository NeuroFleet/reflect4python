#!/usr/bin/python

from twisted.internet.defer import inlineCallbacks
from twisted.logger import Logger

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession
from autobahn.wamp.exception import ApplicationError

################################################################################

def pubsub(*args, **kwargs):
    def do_apply(handler, channel):
        if not hasattr(handler, '_pubsub'):
            setattr(handler, '_pubsub', [])

        if channel not in handler._pubsub:
            handler._pubsub.append(channel)

        return handler

    return lambda hnd: do_apply(hnd, *args, **kwargs)

#*******************************************************************************

def socket_rpc(*args, **kwargs):
    def do_apply(handler, address):
        if not hasattr(handler, '_rpc'):
            setattr(handler, '_rpc', [])

        if address not in handler._rpc:
            handler._rpc.append(address)

        return handler

    return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

class Authority(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
       print("Authority.onJoin({})".format(details))
       try:
           yield self.register(self.authorize, 'authoritize')
           print("Authority: authorizer registered")
       except Exception as e:
           print("Authority: failed to register authorizer procedure ({})".format(e))

    def authorize(self, session, uri, action):
       print("Authority.authorize({}, {}, {})".format(session, uri, action))
       return True

################################################################################

class Registry(object):
    def __init__(self, parent, alias, *args, **kwargs):
        self._prn = parent
        self._key = alias

        self._hnd = {}
        self._lst = {}

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    parent  = property(lambda self: self._prn)
    alias   = property(lambda self: self._key)

    factory = property(lambda self: self._hnd)
    listing = property(lambda self: self._lst)

    log     = property(lambda self: self.parent.log)

    #***************************************************************************

    namespace = property(lambda self: '.'.join([x for x in (
        self.parent.namespace, self.prefix, self.alias
    ) if x]))

    def rpath(self, *parts): return '.'.join([self.namespace]+[x for x in parts])

    #***************************************************************************

    @socket_rpc('create')
    def create(self, provider, narrow, config):
        if narrow not in self.listing:
            if provider in self.factory:
                hnd = self.factory[provider]

                obj = hnd(self, narrow, config)

                self.listing[narrow] = obj

        return self.listing.get(narrow, None)

    @socket_rpc('read')
    def read(self, narrow):
        return self.listing.get(narrow, None)

    @socket_rpc('update')
    def update(self, narrow, config):
        if narrow in self.listing:
            self.listing[narrow].update(config)

    @socket_rpc('delete')
    def delete(self, narrow):
        if narrow in self.listing:
            del self.listing[narrow]

################################################################################

class Cortex(Registry):
    prefix  = 'lobes'

    #***************************************************************************

    @pubsub('lymbic.clock.tick')
    def heartbeat(self, counter):
        self.log.info("published to 'lymbic.clock.tick' with counter {counter}", counter=counter)

    @pubsub('lymbic.pneumo.respire')
    def pneumo_respire(self):
        self.log.info("published to 'lymbic.pneumo.respire'.")

    @pubsub('lymbic.pneumo.expire')
    def pneumo_expire(self):
        self.log.info("published to 'lymbic.pneumo.expire'.")

    #***************************************************************************

    @socket_rpc('add2')
    def add2(self, x, y):
        self.log.info("add2() called with {x} and {y}", x=x, y=y)

        return x + y

################################################################################

class Domain(Registry):
    prefix = 'domains'

    #***************************************************************************

    @pubsub('lymbic.clock.tick')
    def heartbeat(self, counter):
        self.log.info("published to 'lymbic.clock.tick' with counter {counter}", counter=counter)

    @pubsub('lymbic.pneumo.respire')
    def pneumo_respire(self):
        self.log.info("published to 'lymbic.pneumo.respire'.")

    @pubsub('lymbic.pneumo.expire')
    def pneumo_expire(self):
        self.log.info("published to 'lymbic.pneumo.expire'.")

    #***************************************************************************

    @socket_rpc('add2')
    def add2(self, x, y):
        self.log.info("add2() called with {x} and {y}", x=x, y=y)

        return x + y

################################################################################

class Lobby(ApplicationSession):
    namespace = 'uchikoma'

    log = Logger()

    #***************************************************************************

    def rpath(self, *parts): return '.'.join([self.namespace]+[x for x in parts])

    #***************************************************************************

    @inlineCallbacks
    def onJoin(self, details):
        targets = [
            Cortex(self, 'leftex'),
            Cortex(self, 'rightex'),
            Cortex(self, 'rightex'),
        ]

        #for key in ('business', 'internet'):
        #    targets += [Domain(self, key)]

        #***********************************************************************

        for reg in targets:
            for key in dir(reg):
                callback = getattr(reg, key, None)

                if callable(callback):
                    for channel in getattr(callback, '_pubsub', []):
                        yield self.subscribe(callback, channel)

                        self.log.info("subscribed to topic '%s'" % channel)

                    for address in getattr(callback, '_rpc', []):
                        alias = reg.rpath(address)

                        yield self.register(callback, alias)

                        self.log.info("procedure %s() registered" % alias)

        #***********************************************************************

        counter = 0
        pneumo  = True

        while True:
            yield self.publish('lymbic.clock.cardio', counter)

            if counter % 10:
                if pneumo:
                    yield self.publish('lymbic.pneumo.respire')
                else:
                    yield self.publish('lymbic.pneumo.expire')

                pneumo = not pneumo

            counter += 1

            yield sleep(0.8)

