from neurochip.core.streams import *

################################################################################

class Remember(NeuroChip):
    require = {
        'genjutsu': ,
    }

    expose = {
        'memory://self/recall':   'memory_recall',
        'memory://self/remember': 'memory_enforce',
    }

    #***************************************************************************

    def prepare(self):
        nerve = self.require('genjutsu://all@%(SPINE_ORGAN)s/%(SPINE_NERVE)s')

        store = self.require('memory://%(DATA_DOMAIN)s@genjutsu/%(SPINE_ORGAN)s/%(SPINE_NERVE)s')
        graph = self.require('memory://%(DATA_DOMAIN)s@%(DATA_VERSE)s/%(DATA_NAMESPACE)s')

    #***************************************************************************

    def memory_enforce(self, *facts):
        pass

    def memory_recall(self, *facts):
        pass

################################################################################

class Witness(NeuroChip):
    require = {
        'genjutsu': 'memory://%(DATA_DOMAIN)s@%(DATA_VERSE)s/%(DATA_NAEMSPACE)s',
        'assess':   [''],
    }

################################################################################

class RememberText(Remember):
    subscribe = {
        'genjustu://all@ear/hear':      'process_text',
        'genjustu://all@converse/hear': 'process_text',
    }

    #***************************************************************************

    def process_text(self, text):
        pass

#*******************************************************************************

class RememberImage(Remember):
    subscribe = {
        'genjustu://all@eye/see': 'process_image',
    }

    #***************************************************************************

    def process_image(self, link):
        pass

#*******************************************************************************

class RememberURL(Remember):
    subscribe = {
        'genjustu://all@web/read':      'process_url',
    }

    #***************************************************************************

    def process_url(self, link):
        pass

#*******************************************************************************

class RememberLearning(Remember):
    subscribe = {
        'genjustu://all@reason/learn':  'process_xri',
    }

    #***************************************************************************

    def process_xri(self, link):
        pass

#*******************************************************************************

class RememberKnowledge(Remember):
    subscribe = {
        'genjustu://all@know/understand': 'process_facts',
    }

    #***************************************************************************

    def process_facts(self, *facts):
        pass

