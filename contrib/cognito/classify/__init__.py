from neurochip.core.streams import *

################################################################################

class Classification(NeuroChip):
    def process_text(self, text):
        pass

    def process_image(self, link):
        pass

    def process_url(self, link):
        pass

    #***************************************************************************

    def process_xri(self, link):
        pass

    def process_facts(self, *facts):
        pass

    #***************************************************************************

    class topology(Spinal):
        subscribe = {
            'genjustu://all@ear/hear':      'process_text',
            'genjustu://all@converse/hear': 'process_text',
            'genjustu://all@eye/see':       'process_image',
            'genjustu://all@web/read':      'process_url',
            'genjustu://all@reason/learn':  'process_xri',
            'genjustu://all@know/consider': 'process_facts',
        }

################################################################################

class Assess(Classification):
    def process_text(self, text):
        pass

    def process_image(self, link):
        pass

    def process_url(self, link):
        pass

    #***************************************************************************

    def process_xri(self, link):
        pass

    def process_facts(self, *facts):
        pass

    #***************************************************************************

    class topology(Classification.topology):
        expose = {
            'assess://self/train':  'training',
            'memory://self/assess': 'evaluate',
        }

#*******************************************************************************

class k_mean(Assess):
    def prepare(self):
        pass

    #***************************************************************************

    def training(self, *facts):
        pass

    #***************************************************************************

    def evaluate(self, *facts):
        pass

#*******************************************************************************

class k_near(Assess):
    def prepare(self):
        pass

    #***************************************************************************

    def training(self, *facts):
        pass

    #***************************************************************************

    def evaluate(self, *facts):
        pass

