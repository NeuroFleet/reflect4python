from uchikoma.connect.flows.abstract import *

from uchikoma.connect.flows import cognito
from uchikoma.connect.flows import lobes

################################################################################

class Converse(Agent):
    def requires(self):
        return [
            lobes.Wernicke(parent=self, alias='undestand'),
            cognito.AIML(parent=self, alias='assist'),
            lobes.Broca(parent=self, alias='express'),
        ]

