from uchikoma.connect.flows.abstract import *

from uchikoma.connect.flows import cognito

################################################################################

class Witness(Body):
    def requires(self):
        return [
            cognito.Observer(parent=self, alias='sense'),
            cognito.Reasoner(parent=self, alias='explainer'),
            cognito.RdfStore(parent=self, alias='experience'),
        ]

#*******************************************************************************

class Behave(Body):
    def requires(self):
        return [
            #cognito.Observer(parent=self, alias='sense'),
            #cognito.Reasoner(parent=self, alias='explainer'),
            #cognito.RdfStore(parent=self, alias='experience'),
        ]

