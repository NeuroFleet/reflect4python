from uchikoma.abstract.anatomy.body import *

################################################################################

class Lung(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

#*******************************************************************************

class Heart(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

#*******************************************************************************

class Liver(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

################################################################################

class Stomach(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

#*******************************************************************************

class Gastric(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

################################################################################

class Hand(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

#*******************************************************************************

class Foot(Organ):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

