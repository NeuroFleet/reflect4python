from uchikoma.abstract.anatomy.spinal import *

################################################################################

class Somato(Nerve):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

################################################################################

class Skin(Nerve):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

