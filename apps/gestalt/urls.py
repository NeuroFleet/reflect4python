from django.contrib import admin ; admin.autodiscover()

from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^admin/',       include(admin.site.urls)),
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^rq/',          include('django_rq.urls')),

    url(r'^auth/',        include('rest_framework.urls', namespace='rest_framework')),

    url(r'^',             include('uchikoma.gestalt.apis')),

    # url(r'^$', 'uchikoma.views.home', name='home'),
)

