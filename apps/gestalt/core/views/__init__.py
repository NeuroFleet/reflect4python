from uchikoma.gestalt.core.shortcuts import *

################################################################################

#@Reactor.register_web('core', r'domains/')
class DomainList(REST.views.APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = Domain.objects.all()
        serializer = DomainSerializer(snippets, many=True)
        return REST_Response(serializer.data)

    def post(self, request, format=None):
        serializer = DomainSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return REST_Response(serializer.data, status=REST_Status.HTTP_201_CREATED)
        return REST_Response(serializer.errors, status=REST_Status.HTTP_400_BAD_REQUEST)

#*******************************************************************************

#@Reactor.register_web('core', r'snippets/(?P<pk>[0-9]+)/')
class DomainDetail(REST.views.APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return Domain.objects.get(pk=pk)
        except Domain.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DomainSerializer(snippet)
        return REST_Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DomainSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return REST_Response(serializer.data)
        return REST_Response(serializer.errors, status=REST_Status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return REST_Response(status=REST_Status.HTTP_204_NO_CONTENT)

################################################################################

#urlpatterns = Reactor.find_vhost('core').urlpatterns

