from uchikoma.gestalt.shortcuts import *

################################################################################

from .models      import *
from .schemas     import *

from .forms       import *
from .serializers import *

################################################################################

@Reactor.register_vhost('vortex', ns='vortex')
class vHost(VirtualHost):
    pass

################################################################################

from functools import wraps

def render_to(tpl):
    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            out = func(request, *args, **kwargs)
            if isinstance(out, dict):
                out = render_to_response(tpl, out, RequestContext(request))
            return out
        return wrapper
    return decorator

