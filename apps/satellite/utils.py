class RegVerse(object):
    def __init__(self, parent=None):
        self._prnt  = None

        self._truth = {}
        self._cases = {}

    def __call__(self, handler, *args, **kwargs):
        narrow,wrapper = self.register(handler, *args, **kwargs)

        if (narrow not in self._truth):
            self._truth[narrow] = handler

        if (narrow not in self._cases):
            self._cases[narrow] = wrapper

    def __getitem__(self, key):   return self._cases.get(key, None)
    def __dict__(self):           return dict([(k, self._cases[k]) for k in self._cases])

################################################################################

class ApiVerse(RegVerse):
    alias = 'api'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class OntologyVerse(RegVerse):
    alias = 'ontology'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class AdapterVerse(RegVerse):
    alias = 'adapter'

    def register(self, handler, alias):
        return alias,handler

################################################################################

class ModelVerse(RegVerse):
    alias = 'model'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class SchemaVerse(RegVerse):
    alias = 'schema'

    def register(self, handler, alias):
        return alias,handler

################################################################################

class vHostVerse(RegVerse):
    alias = 'vhost'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class RouteVerse(RegVerse):
    alias = 'route'

    def register(self, handler, pattern, verbs=[]):
        return pattern,handler

#*******************************************************************************

class ViewVerse(RegVerse):
    alias = 'view'

    def register(self, handler, pattern, **arguments):
        return pattern,handler

################################################################################

class TaskVerse(RegVerse):
    alias = 'task'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class ProcessVerse(RegVerse):
    alias = 'process'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class FlowVerse(RegVerse):
    alias = 'flow'

    def register(self, handler, alias):
        return alias,handler

################################################################################

class BadgeVerse(RegVerse):
    alias = 'badge'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class ContainerVerse(RegVerse):
    alias = 'container'

    def register(self, handler, alias):
        return alias,handler

#*******************************************************************************

class WidgetVerse(RegVerse):
    alias = 'widget'

    def register(self, handler, alias):
        return alias,handler

################################################################################

class Registrar(object):
    def __init__(self):
        self._reg = {}

        for alias,handler in dict(
            api       = ApiVerse,
            ontology  = OntologyVerse,
            adapter   = AdapterVerse,

            model     = ModelVerse,
            schema    = SchemaVerse,

            vhost     = vHostVerse,
            route     = RouteVerse,
            view      = ViewVerse,

            task      = TaskVerse,
            process   = ProcessVerse,
            flow      = FlowVerse,

            badge     = BadgeVerse,
            container = ContainerVerse,
            widget    = WidgetVerse,
        ).iteritems():
            vrs = handler(self)

            setattr(self, vrs.alias, vrs)

register = Registrar()

