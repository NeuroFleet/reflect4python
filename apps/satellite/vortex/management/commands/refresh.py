#!/usr/bin/env python

import os, sys, time, logging
import glob, json, yaml, requests

from slackclient import SlackClient
from pymongo import MongoClient

from django.core.management.base import BaseCommand, CommandError

from uchikoma.settings import FILES, NEUROCHIP

################################################################################

from uchikoma.vortex.models import *
from uchikoma.vortex.tasks import *

class Command(BaseCommand):
    help = 'Synchronize Linked Data sources.'

    def add_arguments(self, parser):
        pass

    db      = property(lambda self: self.coll)

    def handle(self, *args, **options):
        self.conn = MongoClient('mongodb://mist:optimum@ds039135.mlab.com:39135/vortex')
        self.coll = self.conn['vortex']

        lst = []

        print "*) Scanning SPARQL endpoints :"

        for ep in SparqlEndpoint.objects.all():
            print "\t-> %(link)s ..." % ep.__dict__

            #scan_sparql.delay(alias=ep.alias)

        print "*) Scanning CKAN hubs :"

        for hub in CKAN_DataHub.objects.all():
            print "\t-> %(realm)s ..." % hub.__dict__

            scan_ckan.delay(alias=hub.alias)

        try:
            print "*) Updating Data Hubs :"

            hubs = requests.get('http://instances.ckan.org/config/instances.json').json()
        except:
            print "#) Error while scanning for new CKAN hubs."

        for entry in hubs:
            hub,st = CKAN_DataHub.objects.get_or_create(alias=entry['id'])

            hub.realm   = entry.get('url-api', entry['url'])
            hub.version = 3

            hub.save()

            if st:
                scan_ckan.delay(alias=hub.alias)

            print "\t-> Adding CKAN Hub '%(title)s' at : %(url)s" % entry

        #try:
        #    self.bot.start()
        #except KeyboardInterrupt:
        #    sys.exit(0)
        #except:
        #    logging.exception('OOPS')

    def process(self, ds):
        ds.persist()

