from django import forms

from .models import *

################################################################################

class ContactForm(forms.Form):
    sender  = forms.EmailField()
    subject = forms.CharField(max_length=100)
    content = forms.CharField(widget=forms.Textarea)

    #cc_myself = forms.BooleanField(required=False)

################################################################################

class TokenizeForm(forms.Form):
    content   = forms.CharField(widget=forms.Textarea)
    cc_myself = forms.BooleanField(required=False)

#*******************************************************************************

class QuestionForm(forms.Form):
    content = forms.CharField(max_length=100)

#*******************************************************************************

class EnrichForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea)

################################################################################

class SparqlForm(forms.ModelForm):
    class Meta:
        model   = SparqlQuery
        exclude = ['alias'] #, 'when']

