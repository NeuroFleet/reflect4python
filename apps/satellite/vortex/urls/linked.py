from django.conf.urls import patterns, include, url

slugs = lambda *fields: '/'.join(['(?P<'+f+'>[^/]+)' for f in fields])

urlpatterns = patterns('uchikoma.satellite.satellite.vortex.views.linked',
    url(r'^$',                                          'homepage'),
    url(r'^ckan/$',                                     'ckan_homepage'),
    url(r'^ckan/'+slugs('hub_id')                 +'$', 'ckan_overview'),
    url(r'^ckan/'+slugs('hub_id','ds_id')         +'$', 'ckan_dataset'),
    url(r'^ckan/'+slugs('hub_id','ds_id','res_id')+'$', 'ckan_resource'),
)

