from django.conf.urls import patterns, include, url

urlpatterns = patterns('uchikoma.satellite.vortex.views.meta',
    url(r'^$',               'homepage'),

    url(r'^tokenize$',       'nlp_tokenize'),
    url(r'^question$',       'qas_sparql'),
    url(r'^enrich$',         'rdf_enrich'),
)

