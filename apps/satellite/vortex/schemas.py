from django_mongoengine import Document, EmbeddedDocument, fields

##################################################################################################

class Comment(EmbeddedDocument):
    created_at = fields.DateTimeField(
        default=datetime.datetime.now, required=True, editable=False,
    )
    author = fields.StringField(verbose_name="Name", max_length=255, required=True)
    email  = fields.EmailField(verbose_name="Email")
    body = fields.StringField(verbose_name="Comment", required=True)

#*******************************************************************************

class Message(EmbeddedDocument):
    created_at = fields.DateTimeField(
        default=datetime.datetime.now, required=True, editable=False,
    )
    author = fields.StringField(verbose_name="Name", max_length=255, required=True)
    email  = fields.EmailField(verbose_name="Email")
    body = fields.StringField(verbose_name="Comment", required=True)

##################################################################################################

class DataSet(Document):
    narrow     = fields.StringField(max_length=255, required=True, primary_key=True)
    resource   = fields.StringField(max_length=255, required=True, primary_key=True)

#*******************************************************************************

class DataResource(Document):
    target     = fields.StringField(max_length=255, required=True, choices=FORMATs)

#*******************************************************************************

class DataSet(Document):
    narrow     = fields.StringField(max_length=255, required=True, primary_key=True)
    source     = property(lambda self: DataSchema.objects.get(pk=self.narrow)
    
    when       = fields.DateTimeField(
        default=datetime.datetime.now, required=True, editable=False,
    )
    target     = fields.StringField(max_length=255, required=True, choices=FORMATs)
    mapping    = fields.DictField(required=True)

