#-*- coding: utf-8 -*-

from uchikoma.satellite.vortex.views.shortcuts import *

from uchikoma.satellite.specs import *

################################################################################

@render_to('tld/meta/pages/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

#*******************************************************************************

@render_to('tld/meta/pages/approach.html')
def approach(request, key):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    cnt = context()

    if key in APPROACHs:
        cnt['approach'] = APPROACHs[key]
    else:
        raise Http404()

    return cnt

################################################################################

def nlp_enrich(cnt):
    import nltk

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    cnt['tokens']   = nltk.word_tokenize(cnt['sentence'])
    cnt['tags']     = nltk.pos_tag(cnt['tokens'])
    cnt['tree']     = nltk.chunk.ne_chunk(cnt['tags'])

    return cnt

#*******************************************************************************

@render_to('tld/meta/pages/tokenize.html')
def nlp_tokenize(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return lunch_program(request, forms.TokenizeForm, nlp_enrich)

################################################################################

def quepy_assess(cnt):
    import quepy

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    return cnt

#*******************************************************************************

@render_to('tld/meta/pages/question.html')
def qas_sparql(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return lunch_program(request, forms.QuestionForm, nlp_enrich, quepy_assess)

################################################################################

def rdf_enrich(cnt):
    import rdflib

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    return cnt

#*******************************************************************************

@render_to('tld/meta/pages/enrich.html')
def rdf_enrich(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return lunch_program(request, forms.EnrichForm, nlp_enrich, rdf_enrich)

