#-*- coding: utf-8 -*-

from uchikoma.satellite.vortex.views.shortcuts import *

################################################################################

@render_to('tld/data/pages/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context(
        opendata={
            'ckan': {
                'hub':       CKAN_DataHub.objects.all(),
                'dataset':   CKAN_DataSet.objects.all(),
                'resource':  CKAN_Resource.objects.all(),
            },
            'rdf': {
                'namespace': RdfNamespace.objects.all(),
                'ontology':  RdfOntology.objects.all(),
            },
            'sparql': {
                'endpoint':  SparqlEndpoint.objects.all(),
                'ontology':  SparqlOntology.objects.all(),
                'query':     SparqlQuery.objects.all(),
            },
        },
    )

