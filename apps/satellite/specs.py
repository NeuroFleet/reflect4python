#-*- coding: utf-8 -*-

from uchikoma.satellite.helpers import *

################################################################################

DEBUG = True

ADMINS = (
    ('TAYAA Med Amine', 'amine@tayaa.me'),
)

TIME_ZONE = 'Africa/Casablanca'
LANGUAGE_CODE = 'en-us'

################################################################################

DEFAULT_HOST = 'uchikoma.satellite.xyz'

SUBDOMAINs = {
    'cdn': dict(
        title = "BackStage",
        icon  = 'gears',
    urlconf='uchikoma.satellite.generic.urls.CDN'),
}

#*******************************************************************************

ROOT_URLCONF = 'uchikoma.satellite.generic.urls.Landing'
WSGI_APPLICATION = 'uchikoma.satellite.wsgi.application'

SECRET_KEY = os.environ.get('SESSION_SECRET', '#$5btppqih8=%ae^#&amp;7en#kyi!vh%he9rg=ed#hm6fnw9^=umc')

AUTH_USER_MODEL = 'connect.Identity'

################################################################################

DNS_SPECs = [
    ('connect', [
        dict(name='connect', icon='plug'),
        dict(name='console', icon='desktop'),
        dict(name='hub',     icon='globe'),
    ]),
    ('vortex', [
        dict(name='meta',     icon='anchor'),
        dict(name='graph',    icon='bolt'),

        dict(name='schema',   icon='sitemap'),
        dict(name='semantic', icon='paw'),
        dict(name='linked',   icon='share-alt'),

        dict(name='data',     icon='database'),
        dict(name='geo',      icon='map-marker'),
        #dict(name='',      icon='cubes'),
        #dict(name='',      icon='cubes'),
    ]),
]

#*******************************************************************************

APPROACHs = {
    'rich': dict(title="Semantic Standarts", heading="for Linked Data", items=[
        dict(
            title="DOAP",
            icon='crosshairs blue', link='http://en.wikipedia.org/wiki/DOAP',
            summary="DOAP (Description of a Project) is an RDF Schema and XML vocabulary to describe software projects, in particular free and open source software. It was created and initially developed by Edd Dumbill to convey semantic information associated with open source software projects.",
        ),dict(
            title="Dublin Core",
            icon='flag red', link='http://en.wikipedia.org/wiki/Dublin_core',
            summary="The Dublin Core Schema is a small set of vocabulary terms that can be used to describe web resources (video, images, web pages, etc.), as well as physical resources such as books or CDs, and objects like artworks.",
        ),dict(
            title="FOAF",
            icon='crosshairs blue', link='http://en.wikipedia.org/wiki/FOAF',
            summary="FOAF (an acronym of Friend of a friend) is a machine-readable ontology describing persons, their activities and their relations to other people and objects. Anyone can use FOAF to describe him- or herself. FOAF allows groups of people to describe social networks without the need for a centralised database.",
        ),dict(
            title="Microformats",
            icon='flag green', link='http://en.wikipedia.org/wiki/Microformat',
            summary="A microformat (sometimes abbreviated μF) is a web-based approach to semantic markup which seeks to re-use existing HTML/XHTML tags to convey metadata[1] and other attributes in web pages and other contexts that support (X)HTML such as RSS.",
        ),dict(
            title="Open Graph protocol",
            icon='facebook blue', link='http://ogp.me/',
            summary="The Open Graph protocol enables any web page to become a rich object in a social graph. For instance, this is used on Facebook to allow any web page to have the same functionality as any other object on Facebook.",
        ),dict(
            title="RDFa",
            icon='code red', link='http://en.wikipedia.org/wiki/RDFa',
            summary="RDFa (or Resource Description Framework in Attributes[1]) is a W3C Recommendation that adds a set of attribute-level extensions to HTML, XHTML and various XML-based document types for embedding rich metadata within Web documents.",
        ),
    ]),
    'ipv6': dict(title="IPv6 Standarts", heading="for Next-Gen networks", items=[
        dict(
            title="Teredo tunneling",
            icon='truck blue', link='http://en.wikipedia.org/wiki/Teredo',
            summary="Teredo operates using a platform independent tunneling protocol designed to provide IPv6 (Internet Protocol version 6) connectivity by encapsulating IPv6 datagram packets within IPv4 User Datagram Protocol (UDP) packets. These datagrams can be routed on the IPv4 Internet and through NAT devices. Other Teredo nodes elsewhere called Teredo relays that have access to the IPv6 network then receive the packets, unencapsulate them, and route them on.\n\nTeredo is designed as a last resort transition technology and is intended to be a temporary measure: in the long term, all IPv6 hosts should use native IPv6 connectivity. Teredo should therefore be disabled when native IPv6 connectivity becomes available.",
        ),dict(
            title="6to4",
            icon='video-camera red', link='http://en.wikipedia.org/wiki/6to4',
            summary="6to4 is an Internet transition mechanism for migrating from IPv4 to IPv6, a system that allows IPv6 packets to be transmitted over an IPv4 network (generally the IPv4 Internet) without the need to configure explicit tunnels. Special relay servers are also in place that allow 6to4 networks to communicate with native IPv6 networks.\n\n6to4 may be used by an individual host, or by a local IPv6 network. When used by a host, it must have a global IPv4 address connected, and the host is responsible for encapsulation of outgoing IPv6 packets and decapsulation of incoming 6to4 packets. If the host is configured to forward packets for other clients, often a local network, it is then a router.",
        ),dict(
            title="Tunnel broker",
            icon='laptop green', link='http://en.wikipedia.org/wiki/TunnelBroker',
            summary="In the context of computer networking, a tunnel broker is a service which provides a network tunnel. These tunnels can provide encapsulated connectivity over existing infrastructure to another infrastructure.\n\nThere are a variety of tunnel brokers, including IPv4 tunnel brokers, though most commonly the term is used to refer to an IPv6 tunnel broker as defined in RFC:3053.\n\nIPv6 tunnel brokers typically provide IPv6 tunnels to sites or end users over IPv4. In general, IPv6 tunnel brokers offer so called 'protocol 41' or proto-41 tunnels. These are tunnels where IPv6 is tunneled directly inside IPv4 packets by having the protocol field set to '41' (IPv6) in the IPv4 packet. In the case of IPv4 tunnel brokers IPv4 tunnels are provided to users by encapsulating IPv4 inside IPv6 as defined in RFC:2473.",
        ),
    ]),
    'anon': dict(title="Online Anonimity", heading="Security first !", items=[
        dict(
            title="Tor",
            icon='laptop blue', link='http://en.wikipedia.org/wiki/Tor',
            summary='Tor (previously an acronym for The Onion Router) is free software for enabling online anonymity and censorship resistance. Tor directs Internet traffic through a free, worldwide, volunteer network consisting of more than five thousand relays to conceal a user\'s location or usage from anyone conducting network surveillance or traffic analysis. Using Tor makes it more difficult to trace Internet activity, including "visits to Web sites, online posts, instant messages, and other communication forms", back to the user and is intended to protect the personal privacy of users, as well as their freedom and ability to conduct confidential business by keeping their internet activities from being monitored. An extract of a Top Secret appraisal by the NSA characterized Tor as "the King of high secure, low latency Internet anonymity" with "no contenders for the throne in waiting".',
        ),dict(
            title="I2P",
            icon='video-camera red', link='http://en.wikipedia.org/wiki/I2P',
            summary="The Invisible Internet Project (I2P) is a computer network layer that allows applications to send messages to each other pseudonymously and securely. Uses include anonymous web surfing, chatting, blogging and file transfers. The software that implements this layer is called an I2P router and a computer running I2P is called an I2P node.\n\nThe software is free and open source and is published under multiple licenses. The name I2P is derived from Invisible Internet Project, which, in pseudo-mathematical notation, is represented as I²P.",
        ),dict(
            title="Friend-to-friend",
            icon='truck green', link='http://en.wikipedia.org/wiki/Friend-to-friend',
            summary="A friend-to-friend (or F2F) computer network is a type of peer-to-peer network in which users only make direct connections with people they know. Passwords or digital signatures can be used for authentication.\n\nUnlike other kinds of private P2P, users in a friend-to-friend network cannot find out who else is participating beyond their own circle of friends, so F2F networks can grow in size without compromising their users' anonymity. Retroshare, WASTE, GNUnet, Freenet and OneSwarm are examples of software that can be used to build F2F networks, though RetroShare is the only one of these configured for friend-to-friend operation by default.\n\nMany F2F networks support indirect anonymous or pseudonymous communication between users who do not know or trust one another. For example, a node in a friend-to-friend overlay can automatically forward a file (or a request for a file) anonymously between two friends, without telling either of them the other's name or IP address. These friends can in turn automatically forward the same file (or request) to their own friends, and so on.",
        ),
    ]),
    'react': dict(title="Responsive Design", heading="with HTML 5, CSS 3", items=[
        dict(
            title="LESS",
            icon='desktop orange', link='http://lesscss.org/',
            summary="Less is a CSS pre-processor, meaning that it extends the CSS language, adding features that allow variables, mixins, functions and many other techniques that allow you to make CSS that is more maintainable, themable and extendable.",
        ),dict(
            title="SASS",
            icon='laptop orange', link='http://sass-lang.com/',
            summary="Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.",
        ),dict(
            title="Foundation",
            icon='building blue', link='http://foundation.zurb.com/',
            summary="The most advanced responsive front-end framework in the world.",
        ),dict(
            title="Ink",
            icon='leaf green', link='http://ink.sapo.pt/',
            summary='In the flex layout model, the children of a flex container can be laid out in any direction, and can "flex" their sizes, either growing to fill unused space or shrinking to avoid overflowing the parent.',
        ),dict(
            title="Twitter Bootstrap",
            icon='twitter blue', link='http://getbootstrap.com/',
            summary="The most popular front-end framework for developing responsive, mobile first projects on the web.",
        ),
    ]),
}

