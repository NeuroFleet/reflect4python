from django.conf.urls import patterns, include, url

ns = 'uchikoma.satellite.connect.views.connect'

from django.contrib import admin
admin.autodiscover()

from neo4django import admin as neo_admin
neo_admin.autodiscover()

urlpatterns = patterns('',
    url(r'^login$',                           ns+'.do_login',       name='login'),
    url(r'^logout$',                          ns+'.do_logout',      name='logOUT'),

    url(r'^auth/(?P<backend>[^/]+)/$',        ns+'.ajax_auth',      name='ajax-auth'),

    url(r'^email/require$',                   ns+'.email_require',  name='require_email'),
    url(r'^email/sent',                       ns+'.email_sent'),

    url(r'^$',                                ns+'.homepage',       name='home'),
    url(r'^profile$',                         ns+'.profile',        name='done'),
    url(r'^(networks|people|skills)$',        ns+'.agora_verse',    name='agora_verse'),
    url(r'^settings$',                        ns+'.settings',       name='config'),

    url(r'^topology/(?P<narrow>[^/]+).json$', ns+'.agora_graph',    name='agora_graph'),

    url(r'',      include('social.apps.django_app.urls',    namespace='social'))
)

