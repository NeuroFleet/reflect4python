#!/usr/bin/env python

import os, sys, time, logging
import operator,math
import glob, json, yaml

from django.core.management.base import BaseCommand, CommandError

from uchikoma.satellite.settings import FILES, NEUROCHIP

from uchikoma.satellite.connect import flows

################################################################################

class UnknownChannel(Exception):
    pass

#*******************************************************************************

class Command(BaseCommand):
    help = 'Manage Cortex nodes & Deploy reflectors on Heroku.'

    def add_arguments(self, parser):
        pass # parser.add_argument('poll_id', nargs='+', type=int)

    target_dir = property(lambda self: NEUROCHIP('agents'))
    def rpath(self, *x): return os.path.join(self.target_dir, *x)

    def handle(self, *args, **options):
        bots = {}

        for key in ['Conan','Loki','Max','Musashi','Proto']:
            hnd = getattr(flows, key, None)

            if hnd:
                entry = hnd(alias=hnd.__name__.lower())

                bots[entry.alias] = entry

        flows.luigi.build(bots.values(), local_scheduler=False)

        #if resp is not None:
        #    configure_app(resp, nrw, **opts)
        #
        #    deploy_app(resp, nrw, **opts)

        # 'UCHIKOMA_SAT': 'hub.uchikoma.satellite.xyz',
        # 'UCHIKOMA_BOT': BOTs[index % len(BOTs)],

