from uchikoma.satellite.platform import *

################################################################################

INSTALLED_APPS = (
    #'neo4django.admin',
    #'neo4django.contenttypes',

    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_extensions',
    'django_mongoengine',
    'djcelery',
    'rest_framework',
    'corsheaders',

    'social.apps.django_app.default',
    #'neo4django.graph_auth',

    'uchikoma.satellite.connect',
    'uchikoma.satellite.vortex',
)

################################################################################

SESSION_ENGINE = 'django_mongoengine.sessions'
SESSION_SERIALIZER = 'django_mongoengine.sessions.BSONSerializer'

#SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    'PAGE_SIZE': 10
}

CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    'amine.tayaa.me',
)
CORS_ORIGIN_REGEX_WHITELIST = (
    '^(https?://)?(\w+\.)?uchikoma\.xyz$',
)

################################################################################

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'uchikoma.satellite.middlewares.vHosting',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.contrib.messages.context_processors.messages',
    'social.apps.django_app.context_processors.backends',

    'uchikoma.satellite.middlewares.Enrich',
)

################################################################################

HOST_MIDDLEWARE_URLCONF_MAP = {
    #"backstage."+DEFAULT_HOST: "uchikoma.satellite.generic.urls.supervisor",
}

for ns,lst in DNS_SPECs:
    for entry in lst:
        if len((entry.get('title', None) or '').strip())==0:
            entry['title'] = entry['name'].capitalize()

        entry['urlconf'] = "uchikoma.satellite.%s.urls.%s" % (ns, entry['name'])

        SUBDOMAINs[entry['name']] = entry

for key in SUBDOMAINs:
    SUBDOMAINs[key]['name']      = key

    SUBDOMAINs[key]['menu_path'] = 'tld/%(name)s/navbar.html' % SUBDOMAINs[key]

    if 'domain' not in SUBDOMAINs[key]:
        SUBDOMAINs[key]['domain'] = "%s.%s" % (SUBDOMAINs[key]['name'], DEFAULT_HOST)

    HOST_MIDDLEWARE_URLCONF_MAP[SUBDOMAINs[key]['domain']] = SUBDOMAINs[key]['urlconf']

################################################################################

AUTHENTICATION_BACKENDS = (
    'social.backends.amazon.AmazonOAuth2',
    #'social.backends.angel.AngelOAuth2',
    #'social.backends.aol.AOLOpenId',
    #'social.backends.appsfuel.AppsfuelOAuth2',
    #'social.backends.beats.BeatsOAuth2',
    #'social.backends.behance.BehanceOAuth2',
    #'social.backends.belgiumeid.BelgiumEIDOpenId',
    'social.backends.bitbucket.BitbucketOAuth',
    #'social.backends.box.BoxOAuth2',
    #'social.backends.clef.ClefOAuth2',
    #'social.backends.coinbase.CoinbaseOAuth2',
    #'social.backends.coursera.CourseraOAuth2',
    #'social.backends.dailymotion.DailymotionOAuth2',
    #'social.backends.deezer.DeezerOAuth2',
    #'social.backends.disqus.DisqusOAuth2',
    #'social.backends.douban.DoubanOAuth2',
    #'social.backends.dropbox.DropboxOAuth',
    'social.backends.dropbox.DropboxOAuth2',
    #'social.backends.eveonline.EVEOnlineOAuth2',
    #'social.backends.evernote.EvernoteSandboxOAuth',
    #'social.backends.facebook.FacebookAppOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    #'social.backends.fedora.FedoraOpenId',
    #'social.backends.fitbit.FitbitOAuth2',
    #'social.backends.flickr.FlickrOAuth',
    #'social.backends.foursquare.FoursquareOAuth2',
    'social.backends.github.GithubOAuth2',
    #'social.backends.google.GoogleOAuth',
    'social.backends.google.GoogleOAuth2',
    #'social.backends.google.GoogleOpenId',
    #'social.backends.google.GooglePlusAuth',
    #'social.backends.google.GoogleOpenIdConnect',
    #'social.backends.instagram.InstagramOAuth2',
    #'social.backends.jawbone.JawboneOAuth2',
    #'social.backends.kakao.KakaoOAuth2',
    #'social.backends.linkedin.LinkedinOAuth',
    'social.backends.linkedin.LinkedinOAuth2',
    #'social.backends.live.LiveOAuth2',
    #'social.backends.livejournal.LiveJournalOpenId',
    #'social.backends.mailru.MailruOAuth2',
    #'social.backends.mendeley.MendeleyOAuth',
    #'social.backends.mendeley.MendeleyOAuth2',
    #'social.backends.mineid.MineIDOAuth2',
    #'social.backends.mixcloud.MixcloudOAuth2',
    #'social.backends.nationbuilder.NationBuilderOAuth2',
    #'social.backends.odnoklassniki.OdnoklassnikiOAuth2',
    #'social.backends.open_id.OpenIdAuth',
    #'social.backends.openstreetmap.OpenStreetMapOAuth',
    #'social.backends.persona.PersonaAuth',
    #'social.backends.podio.PodioOAuth2',
    #'social.backends.rdio.RdioOAuth1',
    #'social.backends.rdio.RdioOAuth2',
    #'social.backends.readability.ReadabilityOAuth',
    #'social.backends.reddit.RedditOAuth2',
    #'social.backends.runkeeper.RunKeeperOAuth2',
    #'social.backends.skyrock.SkyrockOAuth',
    #'social.backends.soundcloud.SoundcloudOAuth2',
    #'social.backends.spotify.SpotifyOAuth2',
    'social.backends.stackoverflow.StackoverflowOAuth2',
    #'social.backends.steam.SteamOpenId',
    #'social.backends.stocktwits.StocktwitsOAuth2',
    #'social.backends.stripe.StripeOAuth2',
    #'social.backends.suse.OpenSUSEOpenId',
    #'social.backends.thisismyjam.ThisIsMyJamOAuth1',
    'social.backends.trello.TrelloOAuth',
    #'social.backends.tripit.TripItOAuth',
    #'social.backends.tumblr.TumblrOAuth',
    'social.backends.twilio.TwilioAuth',
    'social.backends.twitter.TwitterOAuth',
    #'social.backends.vk.VKOAuth2',
    #'social.backends.weibo.WeiboOAuth2',
    #'social.backends.wunderlist.WunderlistOAuth2',
    #'social.backends.xing.XingOAuth',
    #'social.backends.yahoo.YahooOAuth',
    'social.backends.yahoo.YahooOpenId',
    #'social.backends.yammer.YammerOAuth2',
    #'social.backends.yandex.YandexOAuth2',
    'social.backends.vimeo.VimeoOAuth1',
    'social.backends.lastfm.LastFmAuth',
    #'social.backends.moves.MovesOAuth2',
    #'social.backends.vend.VendOAuth2',
    'social.backends.email.EmailAuth',
    'social.backends.username.UsernameAuth',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'uchikoma.satellite.connect.pipeline.require_email',
    'social.pipeline.mail.mail_validation',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.debug.debug',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'social.pipeline.debug.debug'
)

################################################################################

URL_PATH = ''

SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'
SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'uchikoma.satellite.connect.mail.send_validation'

SOCIAL_AUTH_EMAIL_FORM_HTML = 'email_signup.html'
SOCIAL_AUTH_USERNAME_FORM_HTML = 'username_signup.html'

# SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['first_name', 'last_name', 'email',
#                                         'username']

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

################################################################################

LIST = (
    'social.backends.amazon.AmazonOAuth2',
    'social.backends.bitbucket.BitbucketOAuth2',
    'social.backends.dropbox.DropboxOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.github.GithubOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.linkedin.LinkedinOAuth2',
    #'social.backends.pocket.PocketAuth',
    'social.backends.stackoverflow.StackoverflowOAuth2',
    'social.backends.trello.TrelloOAuth',
    'social.backends.twilio.TwilioAuth',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.yahoo.YahooOpenId',
)

#*******************************************************************************

SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/userinfo.profile'
]

#*******************************************************************************

SOCIAL_AUTH_TWITTER_KEY    = os.environ.get('API_TWITTER_KEY', 'XEi7Wj41d16Jrq3sMvo9ICPpR')
SOCIAL_AUTH_TWITTER_SECRET = os.environ.get('API_TWITTER_PKI', 'bCKgj20QqgKJQsyuReEhC0N7TTUSJqozVvEovK2LVndZY8beLk')

#*******************************************************************************

SOCIAL_AUTH_FACEBOOK_KEY = os.environ.get('API_FACEBOOK_KEY', '1761961887348597')
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ.get('API_FACEBOOK_PKI', '82e780031dd3b97e21cdf4762bd4b041')

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'ru_RU',
  'fields': 'id, name, email, age_range'
}

#*******************************************************************************

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ.get('API_GOOGLE_KEY', '491752084902-cnj66fo100i84lmpvanbmpac0mlln528.apps.googleusercontent.com')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ.get('API_GOOGLE_PKI', 'JK-QVOWAXQ_zT_fdVzhx7lEj')

#*******************************************************************************

SOCIAL_AUTH_GITHUB_KEY = os.environ.get('API_GITHUB_KEY', 'd2c35ed24d9b8ae3aa0f')
SOCIAL_AUTH_GITHUB_SECRET = os.environ.get('API_GITHUB_PKI', 'eb4bd61f67a4936ea40cbcb5bcfab782a8d44165')

#SOCIAL_AUTH_GITHUB_SCOPE = [...]

#*******************************************************************************

SOCIAL_AUTH_BITBUCKET_OAUTH2_KEY = os.environ.get('API_BITBUCKET_KEY', '38vnfDrwVm2v4mBH8a')
SOCIAL_AUTH_BITBUCKET_OAUTH2_SECRET = os.environ.get('API_BITBUCKET_PKI', 'XJmSMbA3R9JnfnvbwZaUjUsj89HsWPb5')

SOCIAL_AUTH_BITBUCKET_USERNAME_AS_ID = True

#*******************************************************************************

SOCIAL_AUTH_STACKOVERFLOW_KEY = os.environ.get('API_STACKOVERFLOW_KEY', '6901')
SOCIAL_AUTH_STACKOVERFLOW_SECRET = os.environ.get('API_STACKOVERFLOW_PKI', 'ZBiWS9iUfNWbH)Xultm93w((')
SOCIAL_AUTH_STACKOVERFLOW_API_KEY = os.environ.get('API_STACKOVERFLOW_TOKEN', ' jmGy7WzpgiMoYOS7iGpOTw(( ')

#*******************************************************************************

SOCIAL_AUTH_DROPBOX_OAUTH2_KEY = os.environ.get('API_DROPBOX_KEY', 'f01f0r4p12xev8x')
SOCIAL_AUTH_DROPBOX_OAUTH2_SECRET = os.environ.get('API_DROPBOX_PKI', 'vr4gjv405sy8mp0')

#*******************************************************************************

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = os.environ.get('API_LINKEDIN_KEY', '77z0xgqi6gep1v')
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = os.environ.get('API_LINKEDIN_PKI', 'nsMuSZGWBEDZdoAS')

# Add email to requested authorizations.
#SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = ['r_basicprofile', 'r_emailaddress', ...]
# Add the fields so they will be requested from linkedin.
#SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = ['email-address', 'headline', 'industry']
# Arrange to add the fields to UserSocialAuth.extra_data
#SOCIAL_AUTH_LINKEDIN_OAUTH2_EXTRA_DATA = [('id', 'id'),
#                                   ('firstName', 'first_name'),
#                                   ('lastName', 'last_name'),
#                                   ('emailAddress', 'email_address'),
#                                   ('headline', 'headline'),
#                                   ('industry', 'industry')
#]

#*******************************************************************************

#SOCIAL_AUTH_AMAZON_KEY = os.environ.get('API_AMAZON_KEY', '')
#SOCIAL_AUTH_AMAZON_SECRET = os.environ.get('API_AMAZON_PKI', '')

#SOCIAL_AUTH_DOCKER_KEY = os.environ.get('API_DOCKER_KEY', '')
#SOCIAL_AUTH_DOCKER_SECRET = os.environ.get('API_DOCKER_PKI', '')

#*******************************************************************************

SOCIAL_AUTH_POCKET_KEY = os.environ.get('API_POCKET_CONSUMER', '53535-4f7a3307e0656d206f53c817')

################################################################################

API_HEROKU_TOKEN = os.environ.get('API_HEROKU_PKI', '641155c1-fc64-46d3-b80d-8dd99423583b')
API_SLACK_TOKEN  = os.environ.get('API_SLACK_PKI',  '')

################################################################################

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

################################################################################

try:
    from uchikoma.satellite.local_settings import *
except ImportError:
    pass

