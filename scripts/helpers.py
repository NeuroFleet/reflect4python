#-*- encoding: utf-8 -*-

from .utils import *

import pycassa

@singleton
class ORM(object):
    def __init__(self):
        self._kvs = {}

    def connect_kv(self, alias, **cfg):
        if alias not in self._kvs:
            self._kvs[alias] = pycassa.ConnectionPool(alias, **cfg)

        return self._kvs[alias]

    def column_family(self, conn, column):
        if conn in self._kvs:
            conn = self._kvs[conn]

            return pycassa.ColumnFamily(conn, column)

@singleton
class Intellect(object):
    def __init__(self):
        self._reg = {}
        self._lst = {}

    def provide(self, *args, **kwargs):
        def do_apply(handler, alias, **config):
            if alias not in self._reg:
                self._reg[alias] = dict(name=alias,
                    config=config,
                    cache={},
                classe=handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def provider(self, alias, *args, **kargs):
        if alias in self._lst:
            return self._lst[alias]
        else:
            if alias in self._reg:
                meta = self._reg[alias]

                context = meta['config'] ; context.update(kwargs)

                resp = meta['classe'](self, alias, *args, **context)

                return resp

        return None
