#-*- encoding: utf-8 -*-

from .libs    import *
from .utils import *
from .helpers import *

################################################################################

poolWEB   = ORM.connect_kv('web_stuff', server_list=['localhost:19160'])

#git_repos = ORM.column_family('web_stuff', 'git_repos')
#videos    = ORM.column_family('web_stuff', 'web_videos')
#bookmarks = ORM.column_family('web_stuff', 'web_bookmarks')
