#-*- encoding: utf-8 -*-

from .libs import *

def singleton(target):
    return target()

################################################################################

class DataProvider(object):
    def __init__(self):
        pass

    def wrap(self, raw, *args, **kwargs):
        return self.Wrapper(self, raw, *args, **kwargs)

#*******************************************************************************

class DataItem(object):
    def __init__(self, provider, raw, *args, **kwargs):
        self._prvd = provider
        self._item = raw

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)
    raw_item = property(lambda self: self._item)

#*******************************************************************************

class DataBridge(object):
    def __init__(self, provider, *args, **kwargs):
        self._prvd = provider

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)

################################################################################

class ApiConnector(DataBridge):
    def initialize(self, *args, **kwargs):
        self._cnx = None

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    @property
    def conn(self):
        if self._cnx is None:
            self._cnx = self.connect()

        return self._cnx

    def reset(self):
        self._cnx = None

#*******************************************************************************

class FileReader(DataBridge):
    def initialize(self, path):
        self._pth = path

    path = property(lambda self: self._pth)

    def read(self):
        return open(self.path).read()
